--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   20:33:49 12/28/2016
-- Design Name:   
-- Module Name:   C:/Users/Jorjo80/cronometro1/TrabajoSed/Contador_reloj_tb.vhd
-- Project Name:  TrabajoSed
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: contador_reloj
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY Contador_reloj_tb IS
END Contador_reloj_tb;
 
ARCHITECTURE behavior OF Contador_reloj_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT contador_reloj
    PORT(
         clk : IN  std_logic;
         reset : IN  std_logic;
         down : IN  std_logic;
			pausa: in std_logic;
			seleccion: in std_logic;
			b1: in std_logic;
			b2: in std_logic;
			b3: in std_logic;
			b4: in std_logic;
         M1 : OUT  std_logic_vector(2 downto 0);
         M0 : OUT  std_logic_vector(3 downto 0);
         S1 : OUT  std_logic_vector(2 downto 0);
         S0 : OUT  std_logic_vector(3 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal reset : std_logic := '0';
   signal down : std_logic := '0';
	signal seleccion: std_logic:= '0';
	signal b1: std_logic:= '0';
	signal b2:std_logic:= '0';
	signal b3: std_logic:= '0';
	signal b4: std_logic:= '0';
	signal pausa: std_logic:= '0';

 	--Outputs
   signal M1 : std_logic_vector(2 downto 0);
   signal M0 : std_logic_vector(3 downto 0);
   signal S1 : std_logic_vector(2 downto 0);
   signal S0 : std_logic_vector(3 downto 0);

   -- Clock period definitions
   constant clk_period : time := 1000000000 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: contador_reloj PORT MAP (
          clk => clk,
          reset => reset,
          down => down,
			 seleccion => seleccion,
			 b1 => b1,
			 b2 => b2,
			 b3 => b3,
			 b4 => b4,
          M1 => M1,
          M0 => M0,
          S1 => S1,
          S0 => S0,
			 pausa => pausa
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '1';
		wait for clk_period/2;
		clk <= '0';
		wait for clk_period/2;
   end process;
	seleccion <= '0' after 0.01*clk_period, '1' after 9.4*clk_period, '0' after 14*clk_period;
	down <= '0' after 0.25*clk_period, '1' after 15*clk_period;
	b1<= '0' after 0.1*clk_period, '1' after 10*clk_period, '0' after 10.5*clk_period;
	b2<= '0' after 0.1*clk_period, '1' after 11*clk_period, '0' after 11.5*clk_period;
	b3<= '0' after 0.1*clk_period, '1' after 12*clk_period, '0' after 12.5*clk_period;
	b4<= '0' after 0.1*clk_period, '1' after 13*clk_period, '0' after 13.5*clk_period;
	pausa <= '0' after 0.01*clk_period, '1' after 5*clk_period, '0' after 8.4*clk_period;
	reset<= '0';
	
   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
			
      wait for 730000 ns;	

      wait for clk_period*73000;

      -- insert stimulus here 

      wait;
   end process;

END;
