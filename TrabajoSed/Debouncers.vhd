----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    09:49:39 01/12/2017 
-- Design Name: 
-- Module Name:    Debouncers - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Debouncers is
    Port ( clock: in std_logic;
				bb1 : in  STD_LOGIC;
           b1s : out  STD_LOGIC);
end Debouncers;

architecture Behavioral of Debouncers is

begin

	process(clock) -- boton 1 debouncer
	variable b1_aux: std_logic_vector(3 downto 0):= "0000";
		begin
			if rising_edge(clock) then
				b1_aux(3):=b1_aux(2);
				b1_aux(2):=b1_aux(1);
				b1_aux(1):=b1_aux(0);
				b1_aux(0):=bb1;		
				if b1_aux = "1111" then  -- Estando los 4 bits a '1', nos indica que el pulsador o bot�n que estemos mirando
					b1s <= '1';				 -- ya se ha estabilizado a 1, y lo mismo para cuando est�n todos a '0'
				end if;
				if b1_aux = "0000" then
					b1s <= '0';
				end if;
			end if;
	end process;
end Behavioral;

