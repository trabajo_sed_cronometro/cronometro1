--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   12:16:32 12/29/2016
-- Design Name:   
-- Module Name:   C:/Users/Jorjo80/cronometro1/TrabajoSed/clk1_tb.vhd
-- Project Name:  TrabajoSed
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: clk1Hz
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY clk1_tb IS
END clk1_tb;
 
ARCHITECTURE behavioral OF clk1_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT clk1Hz
    PORT(
         entrada : IN  std_logic;
         reset : IN  std_logic;
         salida : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal entrada : std_logic := '0';
   signal reset : std_logic := '0';

 	--Outputs
   signal salida : std_logic;
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
   constant clk_period : time := 20 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: clk1Hz PORT MAP (
          entrada => entrada,
          reset => reset,
          salida => salida
        );

   -- Clock process definitions
   clk_process :process
   begin
		
		entrada <= '0';
		wait for clk_period/2;
		entrada <= '1';
		wait for clk_period/2;
   end process;
   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
    	reset <='1'; -- condiciones iniciales
		wait for 100 ns;
		reset <='0'; -- empezamos

      -- insert stimulus here 

      wait;
   end process;

END;
