--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   16:31:02 12/31/2016
-- Design Name:   
-- Module Name:   C:/Users/Edu F/sed/cronometro1/TrabajoSed/clk200Hz_tb.vhd
-- Project Name:  TrabajoSed
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: clk200Hz
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
ENTITY clk200Hz_tb IS
END clk200Hz_tb;
 
ARCHITECTURE behavior OF clk200Hz_tb IS
    COMPONENT clk200Hz
    PORT(
        entrada : IN  std_logic;
        reset   : IN  std_logic;
        salida  : OUT std_logic
    );
    END COMPONENT;
 
    -- Entradas
    signal entrada : std_logic := '0';
    signal reset   : std_logic := '0';
    -- Salidas
    signal salida  : std_logic;
    constant clk_period : time := 20 ns; 
BEGIN
    -- Instancia de la unidad bajo prueba.
    uut: clk200Hz PORT MAP (
        entrada => entrada,
        reset   => reset,
        salida  => salida
    );
 
    -- Definici�n del reloj.
    clk_process :process
        begin
        entrada <= '0';
        wait for clk_period / 2;
        entrada <= '1';
        wait for clk_period / 2;
    end process;
 
    -- Procesamiento de est�mulos.
    stim_proc: process
    begin
        reset <= '1'; -- Condiciones iniciales.
        wait for 100 ns;
        reset <= '0'; -- �A trabajar!
        wait;
    end process;
END;