----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    12:50:14 12/12/2016 
-- Design Name: 
-- Module Name:    contador_reloj - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_1164.ALL;


entity contador_reloj is
    Port ( clk : in  STD_LOGIC;
           reset : in  STD_LOGIC;
			  down  : in  STD_LOGIC;
			  pausa : in std_logic;
			  seleccion: in std_logic;
			  b1: in std_logic;
			  b2: in std_logic;
			  b3: in std_logic;
			  b4: in std_logic;
				M1   : OUT STD_LOGIC_VECTOR(2 DOWNTO 0); --Segundo digito del minuto.
				M0   : OUT STD_LOGIC_VECTOR(3 DOWNTO 0); --Primer digito del minuto.
				S1   : OUT STD_LOGIC_VECTOR(2 DOWNTO 0); --Segundo digito de los segundos.
				S0   : OUT STD_LOGIC_VECTOR(3 DOWNTO 0)  --Primer digito de los segundos.
	);
end contador_reloj;

architecture Behavioral of contador_reloj is
	signal ss1: UNSIGNED(2 downto 0) := "000" ;
	signal ss0: UNSIGNED(3 downto 0) := "0000";
	signal mm1: UNSIGNED(2 downto 0) := "000" ;
	signal mm0: UNSIGNED(3 downto 0) := "0000";
	signal sel: std_logic:= '1';
begin
	reloj: process (clk, reset,pausa)
	begin
		if reset = '1' then
			mm1 <= "000" ;
			mm0 <= "0000";
			ss1 <= "000" ;
			ss0 <= "0000";
		
		elsif pausa = '1' then -- Modo pausa, pausamos el contador
			mm1 <= mm1;
			mm0 <= mm0;
			ss1 <= ss1;
			ss0 <= ss0;
		
		elsif clk'event and clk='1' then
			if seleccion = '0' then
				sel<='1';
				if down = '0' then --CUENTA HACIA ALANTE 
				--XX:X(X+1)
					ss0 <= ss0 + 1;
				
				--XX:X9==>XX:(X+1)0
					if ss0 = 9 then
						ss1 <= ss1 + 1;
						ss0 <= "0000";
					end if;
				
				-- XX:59==>X(X+1):0X
					if ss1 = 5 and ss0 = 9 then
						mm0 <= mm0 + 1;
						ss1 <= "000";
					end if;
				
				--X9:59==>(X+1)0:XX
					if mm0 = 9 and ss1=5 and ss0=9 then
						mm1 <= mm1 + 1;
						mm0 <= "0000";
					end if;
				
				
				-- 59:59==>00:00
					if mm1 = 5 and mm0 = 9 and ss1 = 5 and ss0 = 9 then
						mm1 <= "000";
						mm0 <= "0000";
						ss1 <= "000";
						ss0 <= "0000";
					end if;
			
			
			--CUENTA HACIA ATRAS
				else
				--XX:XX==>XX:X(X-1)
					ss0<=ss0 - 1;
				
				--XX:X0
					if ss0 = 0 then -- evitamos que vaya incorrectamente a 1001
						ss0<="1001";
					-- XX:00==>X(X-1):5X
						if ss1= 0 then
							mm0<= mm0 - 1;
							ss1<= "101";
									
					--XX:(X!=0)0==>XX:(X-1)9
						else
							ss1<= ss1 -1;
							ss0<="1001";
						end if;
					end if;
				
				--X0:00==>(X-1)9:XX
					if mm0 = 0 and ss1 =0 and ss0 =0 then
						mm1 <= mm1 -1;
						mm0 <= "1001";
					end if;
				
				--00:00==>59:59
					if mm1 = 0 and mm0 = 0 and ss1 = 0 and ss0 = 0 then
						mm1 <= "101";
						mm0 <= "1001";
						ss1 <= "101";
						ss0 <= "1001";
					end if;
				
				end if;
			elsif seleccion = '1' then -- entramos a modo selecci�n del tiempo desde el que empezaremos a contar
				if sel = '1' then
					mm1 <= "000" ;
					mm0 <= "0000";
					ss1 <= "000" ;
					ss0 <= "0000";
					sel<= '0';
				end if;
				if b1='1' then -- sumamos 1 a las unidades de segundos
					ss0<= ss0+1;
					if ss0 = 9 then
						ss0 <= "0000";
						ss1 <= ss1 + 1;
					end if;
				
				elsif b2= '1' then -- sumamos 1 a las decenas de segundos
					ss1 <= ss1 + 1;
					if ss1 > 5 then
						ss1 <= "000";
						mm0 <= mm0 + 1;
					end if;
			
				elsif b3= '1' then -- sumamos 1 a las unidades de minutos
					mm0<= mm0+1;
					if mm0 > 9 then
						mm0 <= "0000";
						mm1 <= mm1 + 1;
					end if;
			
				elsif b4= '1' then -- sumamos 1 a las decenas de minutos
					mm1 <= mm1 + 1;
					if mm1 > 5 then
						ss1 <= "000";
						ss0 <= "0000";
						mm0 <= "0000";
						mm1 <= "000";
					end if;
				end if;
			end if;
	end if;
end process;
	
	--Asignaci�n de se�ales.
	S0 <= STD_LOGIC_VECTOR(ss0);
	S1 <= STD_LOGIC_VECTOR(ss1);	
	M0 <= STD_LOGIC_VECTOR(mm0);
	M1 <= STD_LOGIC_VECTOR(mm1);

end Behavioral;



