--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   20:08:33 12/28/2016
-- Design Name:   
-- Module Name:   C:/Users/Jorjo80/cronometro1/TrabajoSed/contador_tb.vhd
-- Project Name:  TrabajoSed
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: contador_reloj
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY contador_tb IS
END contador_tb;
 
ARCHITECTURE behavior OF contador_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT contador_reloj
    PORT(
         clk : IN  std_logic;
         reset : IN  std_logic;
         down : IN  std_logic;
         Segundo0 : OUT  std_logic_vector(3 downto 0);
         Segundo1 : OUT  std_logic_vector(2 downto 0);
         Minuto0 : OUT  std_logic_vector(3 downto 0);
         Minuto1 : OUT  std_logic_vector(2 downto 0);

        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal reset : std_logic := '0';
   signal down : std_logic := '0';

 	--Outputs
   signal Segundo0 : std_logic_vector(3 downto 0);
   signal Segundo1 : std_logic_vector(2 downto 0);
   signal Minuto0 : std_logic_vector(3 downto 0);
   signal Minuto1 : std_logic_vector(2 downto 0);

   -- Clock period definitions
   constant clk_period : time := 1 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: cronometro PORT MAP (
          clk => clk,
          reset => reset,
          down => down,
          Segundo0 => Segundo0,
          Segundo1 => Segundo1,
          Minuto0 => Minuto0,
          Minuto1 => Minuto1
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
	down <= '0' after 0.25 * CLK_PERIOD, '1' after 360.25 * CLK_PERIOD;
	reset<= '0';
	
	
   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 730000 ns;	

      wait for clk_period*73000;

      -- insert stimulus here 

      wait;
   end process;

END;