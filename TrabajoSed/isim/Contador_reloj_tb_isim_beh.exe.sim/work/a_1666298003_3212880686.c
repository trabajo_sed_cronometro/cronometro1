/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "C:/Users/Jorjo80/cronometro1/TrabajoSed/contador_reloj.vhd";
extern char *IEEE_P_1242562249;

unsigned char ieee_p_1242562249_sub_1781507893_1035706684(char *, char *, char *, int );
unsigned char ieee_p_1242562249_sub_1781543830_1035706684(char *, char *, char *, int );
char *ieee_p_1242562249_sub_1919365254_1035706684(char *, char *, char *, char *, int );
char *ieee_p_1242562249_sub_1919437128_1035706684(char *, char *, char *, char *, int );


static void work_a_1666298003_3212880686_p_0(char *t0)
{
    char t15[16];
    char *t1;
    char *t2;
    unsigned char t3;
    unsigned char t4;
    char *t5;
    char *t6;
    char *t7;
    char *t8;
    char *t9;
    char *t10;
    unsigned char t11;
    unsigned char t12;
    unsigned char t13;
    unsigned char t14;
    unsigned int t16;
    unsigned int t17;
    char *t18;
    char *t19;
    char *t20;
    char *t21;
    char *t22;
    char *t23;
    char *t24;
    unsigned char t25;

LAB0:    xsi_set_current_line(51, ng0);
    t1 = (t0 + 1192U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t4 = (t3 == (unsigned char)3);
    if (t4 != 0)
        goto LAB2;

LAB4:    t1 = (t0 + 1512U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t4 = (t3 == (unsigned char)3);
    if (t4 != 0)
        goto LAB5;

LAB6:    t1 = (t0 + 992U);
    t4 = xsi_signal_has_event(t1);
    if (t4 == 1)
        goto LAB9;

LAB10:    t3 = (unsigned char)0;

LAB11:    if (t3 != 0)
        goto LAB7;

LAB8:
LAB3:    t1 = (t0 + 6224);
    *((int *)t1) = 1;

LAB1:    return;
LAB2:    xsi_set_current_line(52, ng0);
    t1 = (t0 + 10176);
    t6 = (t0 + 6368);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    t9 = (t8 + 56U);
    t10 = *((char **)t9);
    memcpy(t10, t1, 3U);
    xsi_driver_first_trans_fast(t6);
    xsi_set_current_line(53, ng0);
    t1 = (t0 + 10179);
    t5 = (t0 + 6432);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t1, 4U);
    xsi_driver_first_trans_fast(t5);
    xsi_set_current_line(54, ng0);
    t1 = (t0 + 10183);
    t5 = (t0 + 6496);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t1, 3U);
    xsi_driver_first_trans_fast(t5);
    xsi_set_current_line(55, ng0);
    t1 = (t0 + 10186);
    t5 = (t0 + 6560);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t1, 4U);
    xsi_driver_first_trans_fast(t5);
    goto LAB3;

LAB5:    xsi_set_current_line(58, ng0);
    t1 = (t0 + 3432U);
    t5 = *((char **)t1);
    t1 = (t0 + 6368);
    t6 = (t1 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t5, 3U);
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(59, ng0);
    t1 = (t0 + 3592U);
    t2 = *((char **)t1);
    t1 = (t0 + 6432);
    t5 = (t1 + 56U);
    t6 = *((char **)t5);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    memcpy(t8, t2, 4U);
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(60, ng0);
    t1 = (t0 + 3112U);
    t2 = *((char **)t1);
    t1 = (t0 + 6496);
    t5 = (t1 + 56U);
    t6 = *((char **)t5);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    memcpy(t8, t2, 3U);
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(61, ng0);
    t1 = (t0 + 3272U);
    t2 = *((char **)t1);
    t1 = (t0 + 6560);
    t5 = (t1 + 56U);
    t6 = *((char **)t5);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    memcpy(t8, t2, 4U);
    xsi_driver_first_trans_fast(t1);
    goto LAB3;

LAB7:    xsi_set_current_line(64, ng0);
    t2 = (t0 + 1672U);
    t6 = *((char **)t2);
    t13 = *((unsigned char *)t6);
    t14 = (t13 == (unsigned char)2);
    if (t14 != 0)
        goto LAB12;

LAB14:    t1 = (t0 + 1672U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t4 = (t3 == (unsigned char)3);
    if (t4 != 0)
        goto LAB91;

LAB92:
LAB13:    goto LAB3;

LAB9:    t2 = (t0 + 1032U);
    t5 = *((char **)t2);
    t11 = *((unsigned char *)t5);
    t12 = (t11 == (unsigned char)3);
    t3 = t12;
    goto LAB11;

LAB12:    xsi_set_current_line(65, ng0);
    t2 = (t0 + 6624);
    t7 = (t2 + 56U);
    t8 = *((char **)t7);
    t9 = (t8 + 56U);
    t10 = *((char **)t9);
    *((unsigned char *)t10) = (unsigned char)3;
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(66, ng0);
    t1 = (t0 + 1352U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t4 = (t3 == (unsigned char)2);
    if (t4 != 0)
        goto LAB15;

LAB17:    xsi_set_current_line(101, ng0);
    t1 = (t0 + 3272U);
    t2 = *((char **)t1);
    t1 = (t0 + 10096U);
    t5 = ieee_p_1242562249_sub_1919437128_1035706684(IEEE_P_1242562249, t15, t2, t1, 1);
    t6 = (t15 + 12U);
    t16 = *((unsigned int *)t6);
    t17 = (1U * t16);
    t3 = (4U != t17);
    if (t3 == 1)
        goto LAB56;

LAB57:    t7 = (t0 + 6560);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    t10 = (t9 + 56U);
    t18 = *((char **)t10);
    memcpy(t18, t5, 4U);
    xsi_driver_first_trans_fast(t7);
    xsi_set_current_line(104, ng0);
    t1 = (t0 + 3272U);
    t2 = *((char **)t1);
    t1 = (t0 + 10096U);
    t3 = ieee_p_1242562249_sub_1781507893_1035706684(IEEE_P_1242562249, t2, t1, 0);
    if (t3 != 0)
        goto LAB58;

LAB60:
LAB59:    xsi_set_current_line(119, ng0);
    t1 = (t0 + 3592U);
    t2 = *((char **)t1);
    t1 = (t0 + 10128U);
    t11 = ieee_p_1242562249_sub_1781507893_1035706684(IEEE_P_1242562249, t2, t1, 0);
    if (t11 == 1)
        goto LAB74;

LAB75:    t4 = (unsigned char)0;

LAB76:    if (t4 == 1)
        goto LAB71;

LAB72:    t3 = (unsigned char)0;

LAB73:    if (t3 != 0)
        goto LAB68;

LAB70:
LAB69:    xsi_set_current_line(125, ng0);
    t1 = (t0 + 3432U);
    t2 = *((char **)t1);
    t1 = (t0 + 10112U);
    t12 = ieee_p_1242562249_sub_1781507893_1035706684(IEEE_P_1242562249, t2, t1, 0);
    if (t12 == 1)
        goto LAB88;

LAB89:    t11 = (unsigned char)0;

LAB90:    if (t11 == 1)
        goto LAB85;

LAB86:    t4 = (unsigned char)0;

LAB87:    if (t4 == 1)
        goto LAB82;

LAB83:    t3 = (unsigned char)0;

LAB84:    if (t3 != 0)
        goto LAB79;

LAB81:
LAB80:
LAB16:    goto LAB13;

LAB15:    xsi_set_current_line(68, ng0);
    t1 = (t0 + 3272U);
    t5 = *((char **)t1);
    t1 = (t0 + 10096U);
    t6 = ieee_p_1242562249_sub_1919365254_1035706684(IEEE_P_1242562249, t15, t5, t1, 1);
    t7 = (t15 + 12U);
    t16 = *((unsigned int *)t7);
    t17 = (1U * t16);
    t11 = (4U != t17);
    if (t11 == 1)
        goto LAB18;

LAB19:    t8 = (t0 + 6560);
    t9 = (t8 + 56U);
    t10 = *((char **)t9);
    t18 = (t10 + 56U);
    t19 = *((char **)t18);
    memcpy(t19, t6, 4U);
    xsi_driver_first_trans_fast(t8);
    xsi_set_current_line(71, ng0);
    t1 = (t0 + 3272U);
    t2 = *((char **)t1);
    t1 = (t0 + 10096U);
    t3 = ieee_p_1242562249_sub_1781507893_1035706684(IEEE_P_1242562249, t2, t1, 9);
    if (t3 != 0)
        goto LAB20;

LAB22:
LAB21:    xsi_set_current_line(77, ng0);
    t1 = (t0 + 3112U);
    t2 = *((char **)t1);
    t1 = (t0 + 10080U);
    t4 = ieee_p_1242562249_sub_1781507893_1035706684(IEEE_P_1242562249, t2, t1, 5);
    if (t4 == 1)
        goto LAB28;

LAB29:    t3 = (unsigned char)0;

LAB30:    if (t3 != 0)
        goto LAB25;

LAB27:
LAB26:    xsi_set_current_line(83, ng0);
    t1 = (t0 + 3592U);
    t2 = *((char **)t1);
    t1 = (t0 + 10128U);
    t11 = ieee_p_1242562249_sub_1781507893_1035706684(IEEE_P_1242562249, t2, t1, 9);
    if (t11 == 1)
        goto LAB39;

LAB40:    t4 = (unsigned char)0;

LAB41:    if (t4 == 1)
        goto LAB36;

LAB37:    t3 = (unsigned char)0;

LAB38:    if (t3 != 0)
        goto LAB33;

LAB35:
LAB34:    xsi_set_current_line(90, ng0);
    t1 = (t0 + 3432U);
    t2 = *((char **)t1);
    t1 = (t0 + 10112U);
    t12 = ieee_p_1242562249_sub_1781507893_1035706684(IEEE_P_1242562249, t2, t1, 5);
    if (t12 == 1)
        goto LAB53;

LAB54:    t11 = (unsigned char)0;

LAB55:    if (t11 == 1)
        goto LAB50;

LAB51:    t4 = (unsigned char)0;

LAB52:    if (t4 == 1)
        goto LAB47;

LAB48:    t3 = (unsigned char)0;

LAB49:    if (t3 != 0)
        goto LAB44;

LAB46:
LAB45:    goto LAB16;

LAB18:    xsi_size_not_matching(4U, t17, 0);
    goto LAB19;

LAB20:    xsi_set_current_line(72, ng0);
    t5 = (t0 + 3112U);
    t6 = *((char **)t5);
    t5 = (t0 + 10080U);
    t7 = ieee_p_1242562249_sub_1919365254_1035706684(IEEE_P_1242562249, t15, t6, t5, 1);
    t8 = (t15 + 12U);
    t16 = *((unsigned int *)t8);
    t17 = (1U * t16);
    t4 = (3U != t17);
    if (t4 == 1)
        goto LAB23;

LAB24:    t9 = (t0 + 6496);
    t10 = (t9 + 56U);
    t18 = *((char **)t10);
    t19 = (t18 + 56U);
    t20 = *((char **)t19);
    memcpy(t20, t7, 3U);
    xsi_driver_first_trans_fast(t9);
    xsi_set_current_line(73, ng0);
    t1 = (t0 + 10190);
    t5 = (t0 + 6560);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t1, 4U);
    xsi_driver_first_trans_fast(t5);
    goto LAB21;

LAB23:    xsi_size_not_matching(3U, t17, 0);
    goto LAB24;

LAB25:    xsi_set_current_line(78, ng0);
    t7 = (t0 + 3592U);
    t8 = *((char **)t7);
    t7 = (t0 + 10128U);
    t9 = ieee_p_1242562249_sub_1919365254_1035706684(IEEE_P_1242562249, t15, t8, t7, 1);
    t10 = (t15 + 12U);
    t16 = *((unsigned int *)t10);
    t17 = (1U * t16);
    t12 = (4U != t17);
    if (t12 == 1)
        goto LAB31;

LAB32:    t18 = (t0 + 6432);
    t19 = (t18 + 56U);
    t20 = *((char **)t19);
    t21 = (t20 + 56U);
    t22 = *((char **)t21);
    memcpy(t22, t9, 4U);
    xsi_driver_first_trans_fast(t18);
    xsi_set_current_line(79, ng0);
    t1 = (t0 + 10194);
    t5 = (t0 + 6496);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t1, 3U);
    xsi_driver_first_trans_fast(t5);
    goto LAB26;

LAB28:    t5 = (t0 + 3272U);
    t6 = *((char **)t5);
    t5 = (t0 + 10096U);
    t11 = ieee_p_1242562249_sub_1781507893_1035706684(IEEE_P_1242562249, t6, t5, 9);
    t3 = t11;
    goto LAB30;

LAB31:    xsi_size_not_matching(4U, t17, 0);
    goto LAB32;

LAB33:    xsi_set_current_line(84, ng0);
    t9 = (t0 + 3432U);
    t10 = *((char **)t9);
    t9 = (t0 + 10112U);
    t18 = ieee_p_1242562249_sub_1919365254_1035706684(IEEE_P_1242562249, t15, t10, t9, 1);
    t19 = (t15 + 12U);
    t16 = *((unsigned int *)t19);
    t17 = (1U * t16);
    t14 = (3U != t17);
    if (t14 == 1)
        goto LAB42;

LAB43:    t20 = (t0 + 6368);
    t21 = (t20 + 56U);
    t22 = *((char **)t21);
    t23 = (t22 + 56U);
    t24 = *((char **)t23);
    memcpy(t24, t18, 3U);
    xsi_driver_first_trans_fast(t20);
    xsi_set_current_line(85, ng0);
    t1 = (t0 + 10197);
    t5 = (t0 + 6432);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t1, 4U);
    xsi_driver_first_trans_fast(t5);
    goto LAB34;

LAB36:    t7 = (t0 + 3272U);
    t8 = *((char **)t7);
    t7 = (t0 + 10096U);
    t13 = ieee_p_1242562249_sub_1781507893_1035706684(IEEE_P_1242562249, t8, t7, 9);
    t3 = t13;
    goto LAB38;

LAB39:    t5 = (t0 + 3112U);
    t6 = *((char **)t5);
    t5 = (t0 + 10080U);
    t12 = ieee_p_1242562249_sub_1781507893_1035706684(IEEE_P_1242562249, t6, t5, 5);
    t4 = t12;
    goto LAB41;

LAB42:    xsi_size_not_matching(3U, t17, 0);
    goto LAB43;

LAB44:    xsi_set_current_line(91, ng0);
    t18 = (t0 + 10201);
    t20 = (t0 + 6368);
    t21 = (t20 + 56U);
    t22 = *((char **)t21);
    t23 = (t22 + 56U);
    t24 = *((char **)t23);
    memcpy(t24, t18, 3U);
    xsi_driver_first_trans_fast(t20);
    xsi_set_current_line(92, ng0);
    t1 = (t0 + 10204);
    t5 = (t0 + 6432);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t1, 4U);
    xsi_driver_first_trans_fast(t5);
    xsi_set_current_line(93, ng0);
    t1 = (t0 + 10208);
    t5 = (t0 + 6496);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t1, 3U);
    xsi_driver_first_trans_fast(t5);
    xsi_set_current_line(94, ng0);
    t1 = (t0 + 10211);
    t5 = (t0 + 6560);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t1, 4U);
    xsi_driver_first_trans_fast(t5);
    goto LAB45;

LAB47:    t9 = (t0 + 3272U);
    t10 = *((char **)t9);
    t9 = (t0 + 10096U);
    t25 = ieee_p_1242562249_sub_1781507893_1035706684(IEEE_P_1242562249, t10, t9, 9);
    t3 = t25;
    goto LAB49;

LAB50:    t7 = (t0 + 3112U);
    t8 = *((char **)t7);
    t7 = (t0 + 10080U);
    t14 = ieee_p_1242562249_sub_1781507893_1035706684(IEEE_P_1242562249, t8, t7, 5);
    t4 = t14;
    goto LAB52;

LAB53:    t5 = (t0 + 3592U);
    t6 = *((char **)t5);
    t5 = (t0 + 10128U);
    t13 = ieee_p_1242562249_sub_1781507893_1035706684(IEEE_P_1242562249, t6, t5, 9);
    t11 = t13;
    goto LAB55;

LAB56:    xsi_size_not_matching(4U, t17, 0);
    goto LAB57;

LAB58:    xsi_set_current_line(105, ng0);
    t5 = (t0 + 10215);
    t7 = (t0 + 6560);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    t10 = (t9 + 56U);
    t18 = *((char **)t10);
    memcpy(t18, t5, 4U);
    xsi_driver_first_trans_fast(t7);
    xsi_set_current_line(107, ng0);
    t1 = (t0 + 3112U);
    t2 = *((char **)t1);
    t1 = (t0 + 10080U);
    t3 = ieee_p_1242562249_sub_1781507893_1035706684(IEEE_P_1242562249, t2, t1, 0);
    if (t3 != 0)
        goto LAB61;

LAB63:    xsi_set_current_line(113, ng0);
    t1 = (t0 + 3112U);
    t2 = *((char **)t1);
    t1 = (t0 + 10080U);
    t5 = ieee_p_1242562249_sub_1919437128_1035706684(IEEE_P_1242562249, t15, t2, t1, 1);
    t6 = (t15 + 12U);
    t16 = *((unsigned int *)t6);
    t17 = (1U * t16);
    t3 = (3U != t17);
    if (t3 == 1)
        goto LAB66;

LAB67:    t7 = (t0 + 6496);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    t10 = (t9 + 56U);
    t18 = *((char **)t10);
    memcpy(t18, t5, 3U);
    xsi_driver_first_trans_fast(t7);
    xsi_set_current_line(114, ng0);
    t1 = (t0 + 10222);
    t5 = (t0 + 6560);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t1, 4U);
    xsi_driver_first_trans_fast(t5);

LAB62:    goto LAB59;

LAB61:    xsi_set_current_line(108, ng0);
    t5 = (t0 + 3592U);
    t6 = *((char **)t5);
    t5 = (t0 + 10128U);
    t7 = ieee_p_1242562249_sub_1919437128_1035706684(IEEE_P_1242562249, t15, t6, t5, 1);
    t8 = (t15 + 12U);
    t16 = *((unsigned int *)t8);
    t17 = (1U * t16);
    t4 = (4U != t17);
    if (t4 == 1)
        goto LAB64;

LAB65:    t9 = (t0 + 6432);
    t10 = (t9 + 56U);
    t18 = *((char **)t10);
    t19 = (t18 + 56U);
    t20 = *((char **)t19);
    memcpy(t20, t7, 4U);
    xsi_driver_first_trans_fast(t9);
    xsi_set_current_line(109, ng0);
    t1 = (t0 + 10219);
    t5 = (t0 + 6496);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t1, 3U);
    xsi_driver_first_trans_fast(t5);
    goto LAB62;

LAB64:    xsi_size_not_matching(4U, t17, 0);
    goto LAB65;

LAB66:    xsi_size_not_matching(3U, t17, 0);
    goto LAB67;

LAB68:    xsi_set_current_line(120, ng0);
    t9 = (t0 + 3432U);
    t10 = *((char **)t9);
    t9 = (t0 + 10112U);
    t18 = ieee_p_1242562249_sub_1919437128_1035706684(IEEE_P_1242562249, t15, t10, t9, 1);
    t19 = (t15 + 12U);
    t16 = *((unsigned int *)t19);
    t17 = (1U * t16);
    t14 = (3U != t17);
    if (t14 == 1)
        goto LAB77;

LAB78:    t20 = (t0 + 6368);
    t21 = (t20 + 56U);
    t22 = *((char **)t21);
    t23 = (t22 + 56U);
    t24 = *((char **)t23);
    memcpy(t24, t18, 3U);
    xsi_driver_first_trans_fast(t20);
    xsi_set_current_line(121, ng0);
    t1 = (t0 + 10226);
    t5 = (t0 + 6432);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t1, 4U);
    xsi_driver_first_trans_fast(t5);
    goto LAB69;

LAB71:    t7 = (t0 + 3272U);
    t8 = *((char **)t7);
    t7 = (t0 + 10096U);
    t13 = ieee_p_1242562249_sub_1781507893_1035706684(IEEE_P_1242562249, t8, t7, 0);
    t3 = t13;
    goto LAB73;

LAB74:    t5 = (t0 + 3112U);
    t6 = *((char **)t5);
    t5 = (t0 + 10080U);
    t12 = ieee_p_1242562249_sub_1781507893_1035706684(IEEE_P_1242562249, t6, t5, 0);
    t4 = t12;
    goto LAB76;

LAB77:    xsi_size_not_matching(3U, t17, 0);
    goto LAB78;

LAB79:    xsi_set_current_line(126, ng0);
    t18 = (t0 + 10230);
    t20 = (t0 + 6368);
    t21 = (t20 + 56U);
    t22 = *((char **)t21);
    t23 = (t22 + 56U);
    t24 = *((char **)t23);
    memcpy(t24, t18, 3U);
    xsi_driver_first_trans_fast(t20);
    xsi_set_current_line(127, ng0);
    t1 = (t0 + 10233);
    t5 = (t0 + 6432);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t1, 4U);
    xsi_driver_first_trans_fast(t5);
    xsi_set_current_line(128, ng0);
    t1 = (t0 + 10237);
    t5 = (t0 + 6496);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t1, 3U);
    xsi_driver_first_trans_fast(t5);
    xsi_set_current_line(129, ng0);
    t1 = (t0 + 10240);
    t5 = (t0 + 6560);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t1, 4U);
    xsi_driver_first_trans_fast(t5);
    goto LAB80;

LAB82:    t9 = (t0 + 3272U);
    t10 = *((char **)t9);
    t9 = (t0 + 10096U);
    t25 = ieee_p_1242562249_sub_1781507893_1035706684(IEEE_P_1242562249, t10, t9, 0);
    t3 = t25;
    goto LAB84;

LAB85:    t7 = (t0 + 3112U);
    t8 = *((char **)t7);
    t7 = (t0 + 10080U);
    t14 = ieee_p_1242562249_sub_1781507893_1035706684(IEEE_P_1242562249, t8, t7, 0);
    t4 = t14;
    goto LAB87;

LAB88:    t5 = (t0 + 3592U);
    t6 = *((char **)t5);
    t5 = (t0 + 10128U);
    t13 = ieee_p_1242562249_sub_1781507893_1035706684(IEEE_P_1242562249, t6, t5, 0);
    t11 = t13;
    goto LAB90;

LAB91:    xsi_set_current_line(134, ng0);
    t1 = (t0 + 3752U);
    t5 = *((char **)t1);
    t11 = *((unsigned char *)t5);
    t12 = (t11 == (unsigned char)3);
    if (t12 != 0)
        goto LAB93;

LAB95:
LAB94:    xsi_set_current_line(141, ng0);
    t1 = (t0 + 1832U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t4 = (t3 == (unsigned char)3);
    if (t4 != 0)
        goto LAB96;

LAB98:    t1 = (t0 + 1992U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t4 = (t3 == (unsigned char)3);
    if (t4 != 0)
        goto LAB106;

LAB107:    t1 = (t0 + 2152U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t4 = (t3 == (unsigned char)3);
    if (t4 != 0)
        goto LAB115;

LAB116:    t1 = (t0 + 2312U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t4 = (t3 == (unsigned char)3);
    if (t4 != 0)
        goto LAB124;

LAB125:
LAB97:    goto LAB13;

LAB93:    xsi_set_current_line(135, ng0);
    t1 = (t0 + 10244);
    t7 = (t0 + 6368);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    t10 = (t9 + 56U);
    t18 = *((char **)t10);
    memcpy(t18, t1, 3U);
    xsi_driver_first_trans_fast(t7);
    xsi_set_current_line(136, ng0);
    t1 = (t0 + 10247);
    t5 = (t0 + 6432);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t1, 4U);
    xsi_driver_first_trans_fast(t5);
    xsi_set_current_line(137, ng0);
    t1 = (t0 + 10251);
    t5 = (t0 + 6496);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t1, 3U);
    xsi_driver_first_trans_fast(t5);
    xsi_set_current_line(138, ng0);
    t1 = (t0 + 10254);
    t5 = (t0 + 6560);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t1, 4U);
    xsi_driver_first_trans_fast(t5);
    xsi_set_current_line(139, ng0);
    t1 = (t0 + 6624);
    t2 = (t1 + 56U);
    t5 = *((char **)t2);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    *((unsigned char *)t7) = (unsigned char)2;
    xsi_driver_first_trans_fast(t1);
    goto LAB94;

LAB96:    xsi_set_current_line(142, ng0);
    t1 = (t0 + 3272U);
    t5 = *((char **)t1);
    t1 = (t0 + 10096U);
    t6 = ieee_p_1242562249_sub_1919365254_1035706684(IEEE_P_1242562249, t15, t5, t1, 1);
    t7 = (t15 + 12U);
    t16 = *((unsigned int *)t7);
    t17 = (1U * t16);
    t11 = (4U != t17);
    if (t11 == 1)
        goto LAB99;

LAB100:    t8 = (t0 + 6560);
    t9 = (t8 + 56U);
    t10 = *((char **)t9);
    t18 = (t10 + 56U);
    t19 = *((char **)t18);
    memcpy(t19, t6, 4U);
    xsi_driver_first_trans_fast(t8);
    xsi_set_current_line(143, ng0);
    t1 = (t0 + 3272U);
    t2 = *((char **)t1);
    t1 = (t0 + 10096U);
    t3 = ieee_p_1242562249_sub_1781507893_1035706684(IEEE_P_1242562249, t2, t1, 9);
    if (t3 != 0)
        goto LAB101;

LAB103:
LAB102:    goto LAB97;

LAB99:    xsi_size_not_matching(4U, t17, 0);
    goto LAB100;

LAB101:    xsi_set_current_line(144, ng0);
    t5 = (t0 + 10258);
    t7 = (t0 + 6560);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    t10 = (t9 + 56U);
    t18 = *((char **)t10);
    memcpy(t18, t5, 4U);
    xsi_driver_first_trans_fast(t7);
    xsi_set_current_line(145, ng0);
    t1 = (t0 + 3112U);
    t2 = *((char **)t1);
    t1 = (t0 + 10080U);
    t5 = ieee_p_1242562249_sub_1919365254_1035706684(IEEE_P_1242562249, t15, t2, t1, 1);
    t6 = (t15 + 12U);
    t16 = *((unsigned int *)t6);
    t17 = (1U * t16);
    t3 = (3U != t17);
    if (t3 == 1)
        goto LAB104;

LAB105:    t7 = (t0 + 6496);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    t10 = (t9 + 56U);
    t18 = *((char **)t10);
    memcpy(t18, t5, 3U);
    xsi_driver_first_trans_fast(t7);
    goto LAB102;

LAB104:    xsi_size_not_matching(3U, t17, 0);
    goto LAB105;

LAB106:    xsi_set_current_line(149, ng0);
    t1 = (t0 + 3112U);
    t5 = *((char **)t1);
    t1 = (t0 + 10080U);
    t6 = ieee_p_1242562249_sub_1919365254_1035706684(IEEE_P_1242562249, t15, t5, t1, 1);
    t7 = (t15 + 12U);
    t16 = *((unsigned int *)t7);
    t17 = (1U * t16);
    t11 = (3U != t17);
    if (t11 == 1)
        goto LAB108;

LAB109:    t8 = (t0 + 6496);
    t9 = (t8 + 56U);
    t10 = *((char **)t9);
    t18 = (t10 + 56U);
    t19 = *((char **)t18);
    memcpy(t19, t6, 3U);
    xsi_driver_first_trans_fast(t8);
    xsi_set_current_line(150, ng0);
    t1 = (t0 + 3112U);
    t2 = *((char **)t1);
    t1 = (t0 + 10080U);
    t3 = ieee_p_1242562249_sub_1781543830_1035706684(IEEE_P_1242562249, t2, t1, 5);
    if (t3 != 0)
        goto LAB110;

LAB112:
LAB111:    goto LAB97;

LAB108:    xsi_size_not_matching(3U, t17, 0);
    goto LAB109;

LAB110:    xsi_set_current_line(151, ng0);
    t5 = (t0 + 10262);
    t7 = (t0 + 6496);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    t10 = (t9 + 56U);
    t18 = *((char **)t10);
    memcpy(t18, t5, 3U);
    xsi_driver_first_trans_fast(t7);
    xsi_set_current_line(152, ng0);
    t1 = (t0 + 3592U);
    t2 = *((char **)t1);
    t1 = (t0 + 10128U);
    t5 = ieee_p_1242562249_sub_1919365254_1035706684(IEEE_P_1242562249, t15, t2, t1, 1);
    t6 = (t15 + 12U);
    t16 = *((unsigned int *)t6);
    t17 = (1U * t16);
    t3 = (4U != t17);
    if (t3 == 1)
        goto LAB113;

LAB114:    t7 = (t0 + 6432);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    t10 = (t9 + 56U);
    t18 = *((char **)t10);
    memcpy(t18, t5, 4U);
    xsi_driver_first_trans_fast(t7);
    goto LAB111;

LAB113:    xsi_size_not_matching(4U, t17, 0);
    goto LAB114;

LAB115:    xsi_set_current_line(156, ng0);
    t1 = (t0 + 3592U);
    t5 = *((char **)t1);
    t1 = (t0 + 10128U);
    t6 = ieee_p_1242562249_sub_1919365254_1035706684(IEEE_P_1242562249, t15, t5, t1, 1);
    t7 = (t15 + 12U);
    t16 = *((unsigned int *)t7);
    t17 = (1U * t16);
    t11 = (4U != t17);
    if (t11 == 1)
        goto LAB117;

LAB118:    t8 = (t0 + 6432);
    t9 = (t8 + 56U);
    t10 = *((char **)t9);
    t18 = (t10 + 56U);
    t19 = *((char **)t18);
    memcpy(t19, t6, 4U);
    xsi_driver_first_trans_fast(t8);
    xsi_set_current_line(157, ng0);
    t1 = (t0 + 3592U);
    t2 = *((char **)t1);
    t1 = (t0 + 10128U);
    t3 = ieee_p_1242562249_sub_1781543830_1035706684(IEEE_P_1242562249, t2, t1, 9);
    if (t3 != 0)
        goto LAB119;

LAB121:
LAB120:    goto LAB97;

LAB117:    xsi_size_not_matching(4U, t17, 0);
    goto LAB118;

LAB119:    xsi_set_current_line(158, ng0);
    t5 = (t0 + 10265);
    t7 = (t0 + 6432);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    t10 = (t9 + 56U);
    t18 = *((char **)t10);
    memcpy(t18, t5, 4U);
    xsi_driver_first_trans_fast(t7);
    xsi_set_current_line(159, ng0);
    t1 = (t0 + 3432U);
    t2 = *((char **)t1);
    t1 = (t0 + 10112U);
    t5 = ieee_p_1242562249_sub_1919365254_1035706684(IEEE_P_1242562249, t15, t2, t1, 1);
    t6 = (t15 + 12U);
    t16 = *((unsigned int *)t6);
    t17 = (1U * t16);
    t3 = (3U != t17);
    if (t3 == 1)
        goto LAB122;

LAB123:    t7 = (t0 + 6368);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    t10 = (t9 + 56U);
    t18 = *((char **)t10);
    memcpy(t18, t5, 3U);
    xsi_driver_first_trans_fast(t7);
    goto LAB120;

LAB122:    xsi_size_not_matching(3U, t17, 0);
    goto LAB123;

LAB124:    xsi_set_current_line(163, ng0);
    t1 = (t0 + 3432U);
    t5 = *((char **)t1);
    t1 = (t0 + 10112U);
    t6 = ieee_p_1242562249_sub_1919365254_1035706684(IEEE_P_1242562249, t15, t5, t1, 1);
    t7 = (t15 + 12U);
    t16 = *((unsigned int *)t7);
    t17 = (1U * t16);
    t11 = (3U != t17);
    if (t11 == 1)
        goto LAB126;

LAB127:    t8 = (t0 + 6368);
    t9 = (t8 + 56U);
    t10 = *((char **)t9);
    t18 = (t10 + 56U);
    t19 = *((char **)t18);
    memcpy(t19, t6, 3U);
    xsi_driver_first_trans_fast(t8);
    xsi_set_current_line(164, ng0);
    t1 = (t0 + 3432U);
    t2 = *((char **)t1);
    t1 = (t0 + 10112U);
    t3 = ieee_p_1242562249_sub_1781543830_1035706684(IEEE_P_1242562249, t2, t1, 5);
    if (t3 != 0)
        goto LAB128;

LAB130:
LAB129:    goto LAB97;

LAB126:    xsi_size_not_matching(3U, t17, 0);
    goto LAB127;

LAB128:    xsi_set_current_line(165, ng0);
    t5 = (t0 + 10269);
    t7 = (t0 + 6496);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    t10 = (t9 + 56U);
    t18 = *((char **)t10);
    memcpy(t18, t5, 3U);
    xsi_driver_first_trans_fast(t7);
    xsi_set_current_line(166, ng0);
    t1 = (t0 + 10272);
    t5 = (t0 + 6560);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t1, 4U);
    xsi_driver_first_trans_fast(t5);
    xsi_set_current_line(167, ng0);
    t1 = (t0 + 10276);
    t5 = (t0 + 6432);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t1, 4U);
    xsi_driver_first_trans_fast(t5);
    xsi_set_current_line(168, ng0);
    t1 = (t0 + 10280);
    t5 = (t0 + 6368);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t1, 3U);
    xsi_driver_first_trans_fast(t5);
    goto LAB129;

}

static void work_a_1666298003_3212880686_p_1(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;

LAB0:    xsi_set_current_line(176, ng0);

LAB3:    t1 = (t0 + 3272U);
    t2 = *((char **)t1);
    t1 = (t0 + 6688);
    t3 = (t1 + 56U);
    t4 = *((char **)t3);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    memcpy(t6, t2, 4U);
    xsi_driver_first_trans_fast_port(t1);

LAB2:    t7 = (t0 + 6240);
    *((int *)t7) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_1666298003_3212880686_p_2(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;

LAB0:    xsi_set_current_line(177, ng0);

LAB3:    t1 = (t0 + 3112U);
    t2 = *((char **)t1);
    t1 = (t0 + 6752);
    t3 = (t1 + 56U);
    t4 = *((char **)t3);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    memcpy(t6, t2, 3U);
    xsi_driver_first_trans_fast_port(t1);

LAB2:    t7 = (t0 + 6256);
    *((int *)t7) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_1666298003_3212880686_p_3(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;

LAB0:    xsi_set_current_line(178, ng0);

LAB3:    t1 = (t0 + 3592U);
    t2 = *((char **)t1);
    t1 = (t0 + 6816);
    t3 = (t1 + 56U);
    t4 = *((char **)t3);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    memcpy(t6, t2, 4U);
    xsi_driver_first_trans_fast_port(t1);

LAB2:    t7 = (t0 + 6272);
    *((int *)t7) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_1666298003_3212880686_p_4(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;

LAB0:    xsi_set_current_line(179, ng0);

LAB3:    t1 = (t0 + 3432U);
    t2 = *((char **)t1);
    t1 = (t0 + 6880);
    t3 = (t1 + 56U);
    t4 = *((char **)t3);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    memcpy(t6, t2, 3U);
    xsi_driver_first_trans_fast_port(t1);

LAB2:    t7 = (t0 + 6288);
    *((int *)t7) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}


extern void work_a_1666298003_3212880686_init()
{
	static char *pe[] = {(void *)work_a_1666298003_3212880686_p_0,(void *)work_a_1666298003_3212880686_p_1,(void *)work_a_1666298003_3212880686_p_2,(void *)work_a_1666298003_3212880686_p_3,(void *)work_a_1666298003_3212880686_p_4};
	xsi_register_didat("work_a_1666298003_3212880686", "isim/Contador_reloj_tb_isim_beh.exe.sim/work/a_1666298003_3212880686.didat");
	xsi_register_executes(pe);
}
