/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "C:/Users/Jorjo80/cronometro1/TrabajoSed/Contador_reloj_tb.vhd";



static void work_a_3084656879_2372691052_p_0(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    int64 t7;
    int64 t8;

LAB0:    t1 = (t0 + 4232U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(103, ng0);
    t2 = (t0 + 6848);
    t3 = (t2 + 56U);
    t4 = *((char **)t3);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    *((unsigned char *)t6) = (unsigned char)3;
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(104, ng0);
    t2 = (t0 + 3248U);
    t3 = *((char **)t2);
    t7 = *((int64 *)t3);
    t8 = (t7 / 2);
    t2 = (t0 + 4040);
    xsi_process_wait(t2, t8);

LAB6:    *((char **)t1) = &&LAB7;

LAB1:    return;
LAB4:    xsi_set_current_line(105, ng0);
    t2 = (t0 + 6848);
    t3 = (t2 + 56U);
    t4 = *((char **)t3);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    *((unsigned char *)t6) = (unsigned char)2;
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(106, ng0);
    t2 = (t0 + 3248U);
    t3 = *((char **)t2);
    t7 = *((int64 *)t3);
    t8 = (t7 / 2);
    t2 = (t0 + 4040);
    xsi_process_wait(t2, t8);

LAB10:    *((char **)t1) = &&LAB11;
    goto LAB1;

LAB5:    goto LAB4;

LAB7:    goto LAB5;

LAB8:    goto LAB2;

LAB9:    goto LAB8;

LAB11:    goto LAB9;

}

static void work_a_3084656879_2372691052_p_1(char *t0)
{
    char *t1;
    char *t2;
    int64 t3;
    int64 t4;
    char *t5;
    char *t6;
    char *t7;
    char *t8;
    char *t9;
    char *t10;
    int64 t11;
    int64 t12;
    char *t13;
    char *t14;
    char *t15;
    char *t16;
    char *t17;
    char *t18;
    int64 t19;
    int64 t20;
    char *t21;
    char *t22;
    char *t23;
    char *t24;
    char *t25;

LAB0:    xsi_set_current_line(108, ng0);

LAB3:    t1 = (t0 + 3248U);
    t2 = *((char **)t1);
    t3 = *((int64 *)t2);
    t4 = (0.010000000000000000 * t3);
    t1 = (t0 + 6912);
    t5 = (t1 + 56U);
    t6 = *((char **)t5);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)2;
    xsi_driver_first_trans_delta(t1, 0U, 1, t4);
    t9 = (t0 + 3248U);
    t10 = *((char **)t9);
    t11 = *((int64 *)t10);
    t12 = (9.4000000000000004 * t11);
    t9 = (t0 + 6912);
    t13 = (t9 + 56U);
    t14 = *((char **)t13);
    t15 = (t14 + 56U);
    t16 = *((char **)t15);
    *((unsigned char *)t16) = (unsigned char)3;
    xsi_driver_subsequent_trans_delta(t9, 0U, 1, t12);
    t17 = (t0 + 3248U);
    t18 = *((char **)t17);
    t19 = *((int64 *)t18);
    t20 = (14 * t19);
    t17 = (t0 + 6912);
    t21 = (t17 + 56U);
    t22 = *((char **)t21);
    t23 = (t22 + 56U);
    t24 = *((char **)t23);
    *((unsigned char *)t24) = (unsigned char)2;
    xsi_driver_subsequent_trans_delta(t17, 0U, 1, t20);
    t25 = (t0 + 6912);
    xsi_driver_intertial_reject(t25, t4, t4);

LAB2:
LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_3084656879_2372691052_p_2(char *t0)
{
    char *t1;
    char *t2;
    int64 t3;
    int64 t4;
    char *t5;
    char *t6;
    char *t7;
    char *t8;
    char *t9;
    char *t10;
    int64 t11;
    int64 t12;
    char *t13;
    char *t14;
    char *t15;
    char *t16;
    char *t17;

LAB0:    xsi_set_current_line(109, ng0);

LAB3:    t1 = (t0 + 3248U);
    t2 = *((char **)t1);
    t3 = *((int64 *)t2);
    t4 = (0.25000000000000000 * t3);
    t1 = (t0 + 6976);
    t5 = (t1 + 56U);
    t6 = *((char **)t5);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)2;
    xsi_driver_first_trans_delta(t1, 0U, 1, t4);
    t9 = (t0 + 3248U);
    t10 = *((char **)t9);
    t11 = *((int64 *)t10);
    t12 = (15 * t11);
    t9 = (t0 + 6976);
    t13 = (t9 + 56U);
    t14 = *((char **)t13);
    t15 = (t14 + 56U);
    t16 = *((char **)t15);
    *((unsigned char *)t16) = (unsigned char)3;
    xsi_driver_subsequent_trans_delta(t9, 0U, 1, t12);
    t17 = (t0 + 6976);
    xsi_driver_intertial_reject(t17, t4, t4);

LAB2:
LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_3084656879_2372691052_p_3(char *t0)
{
    char *t1;
    char *t2;
    int64 t3;
    int64 t4;
    char *t5;
    char *t6;
    char *t7;
    char *t8;
    char *t9;
    char *t10;
    int64 t11;
    int64 t12;
    char *t13;
    char *t14;
    char *t15;
    char *t16;
    char *t17;
    char *t18;
    int64 t19;
    int64 t20;
    char *t21;
    char *t22;
    char *t23;
    char *t24;
    char *t25;

LAB0:    xsi_set_current_line(110, ng0);

LAB3:    t1 = (t0 + 3248U);
    t2 = *((char **)t1);
    t3 = *((int64 *)t2);
    t4 = (0.10000000000000001 * t3);
    t1 = (t0 + 7040);
    t5 = (t1 + 56U);
    t6 = *((char **)t5);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)2;
    xsi_driver_first_trans_delta(t1, 0U, 1, t4);
    t9 = (t0 + 3248U);
    t10 = *((char **)t9);
    t11 = *((int64 *)t10);
    t12 = (10 * t11);
    t9 = (t0 + 7040);
    t13 = (t9 + 56U);
    t14 = *((char **)t13);
    t15 = (t14 + 56U);
    t16 = *((char **)t15);
    *((unsigned char *)t16) = (unsigned char)3;
    xsi_driver_subsequent_trans_delta(t9, 0U, 1, t12);
    t17 = (t0 + 3248U);
    t18 = *((char **)t17);
    t19 = *((int64 *)t18);
    t20 = (10.500000000000000 * t19);
    t17 = (t0 + 7040);
    t21 = (t17 + 56U);
    t22 = *((char **)t21);
    t23 = (t22 + 56U);
    t24 = *((char **)t23);
    *((unsigned char *)t24) = (unsigned char)2;
    xsi_driver_subsequent_trans_delta(t17, 0U, 1, t20);
    t25 = (t0 + 7040);
    xsi_driver_intertial_reject(t25, t4, t4);

LAB2:
LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_3084656879_2372691052_p_4(char *t0)
{
    char *t1;
    char *t2;
    int64 t3;
    int64 t4;
    char *t5;
    char *t6;
    char *t7;
    char *t8;
    char *t9;
    char *t10;
    int64 t11;
    int64 t12;
    char *t13;
    char *t14;
    char *t15;
    char *t16;
    char *t17;
    char *t18;
    int64 t19;
    int64 t20;
    char *t21;
    char *t22;
    char *t23;
    char *t24;
    char *t25;

LAB0:    xsi_set_current_line(111, ng0);

LAB3:    t1 = (t0 + 3248U);
    t2 = *((char **)t1);
    t3 = *((int64 *)t2);
    t4 = (0.10000000000000001 * t3);
    t1 = (t0 + 7104);
    t5 = (t1 + 56U);
    t6 = *((char **)t5);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)2;
    xsi_driver_first_trans_delta(t1, 0U, 1, t4);
    t9 = (t0 + 3248U);
    t10 = *((char **)t9);
    t11 = *((int64 *)t10);
    t12 = (11 * t11);
    t9 = (t0 + 7104);
    t13 = (t9 + 56U);
    t14 = *((char **)t13);
    t15 = (t14 + 56U);
    t16 = *((char **)t15);
    *((unsigned char *)t16) = (unsigned char)3;
    xsi_driver_subsequent_trans_delta(t9, 0U, 1, t12);
    t17 = (t0 + 3248U);
    t18 = *((char **)t17);
    t19 = *((int64 *)t18);
    t20 = (11.500000000000000 * t19);
    t17 = (t0 + 7104);
    t21 = (t17 + 56U);
    t22 = *((char **)t21);
    t23 = (t22 + 56U);
    t24 = *((char **)t23);
    *((unsigned char *)t24) = (unsigned char)2;
    xsi_driver_subsequent_trans_delta(t17, 0U, 1, t20);
    t25 = (t0 + 7104);
    xsi_driver_intertial_reject(t25, t4, t4);

LAB2:
LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_3084656879_2372691052_p_5(char *t0)
{
    char *t1;
    char *t2;
    int64 t3;
    int64 t4;
    char *t5;
    char *t6;
    char *t7;
    char *t8;
    char *t9;
    char *t10;
    int64 t11;
    int64 t12;
    char *t13;
    char *t14;
    char *t15;
    char *t16;
    char *t17;
    char *t18;
    int64 t19;
    int64 t20;
    char *t21;
    char *t22;
    char *t23;
    char *t24;
    char *t25;

LAB0:    xsi_set_current_line(112, ng0);

LAB3:    t1 = (t0 + 3248U);
    t2 = *((char **)t1);
    t3 = *((int64 *)t2);
    t4 = (0.10000000000000001 * t3);
    t1 = (t0 + 7168);
    t5 = (t1 + 56U);
    t6 = *((char **)t5);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)2;
    xsi_driver_first_trans_delta(t1, 0U, 1, t4);
    t9 = (t0 + 3248U);
    t10 = *((char **)t9);
    t11 = *((int64 *)t10);
    t12 = (12 * t11);
    t9 = (t0 + 7168);
    t13 = (t9 + 56U);
    t14 = *((char **)t13);
    t15 = (t14 + 56U);
    t16 = *((char **)t15);
    *((unsigned char *)t16) = (unsigned char)3;
    xsi_driver_subsequent_trans_delta(t9, 0U, 1, t12);
    t17 = (t0 + 3248U);
    t18 = *((char **)t17);
    t19 = *((int64 *)t18);
    t20 = (12.500000000000000 * t19);
    t17 = (t0 + 7168);
    t21 = (t17 + 56U);
    t22 = *((char **)t21);
    t23 = (t22 + 56U);
    t24 = *((char **)t23);
    *((unsigned char *)t24) = (unsigned char)2;
    xsi_driver_subsequent_trans_delta(t17, 0U, 1, t20);
    t25 = (t0 + 7168);
    xsi_driver_intertial_reject(t25, t4, t4);

LAB2:
LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_3084656879_2372691052_p_6(char *t0)
{
    char *t1;
    char *t2;
    int64 t3;
    int64 t4;
    char *t5;
    char *t6;
    char *t7;
    char *t8;
    char *t9;
    char *t10;
    int64 t11;
    int64 t12;
    char *t13;
    char *t14;
    char *t15;
    char *t16;
    char *t17;
    char *t18;
    int64 t19;
    int64 t20;
    char *t21;
    char *t22;
    char *t23;
    char *t24;
    char *t25;

LAB0:    xsi_set_current_line(113, ng0);

LAB3:    t1 = (t0 + 3248U);
    t2 = *((char **)t1);
    t3 = *((int64 *)t2);
    t4 = (0.10000000000000001 * t3);
    t1 = (t0 + 7232);
    t5 = (t1 + 56U);
    t6 = *((char **)t5);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)2;
    xsi_driver_first_trans_delta(t1, 0U, 1, t4);
    t9 = (t0 + 3248U);
    t10 = *((char **)t9);
    t11 = *((int64 *)t10);
    t12 = (13 * t11);
    t9 = (t0 + 7232);
    t13 = (t9 + 56U);
    t14 = *((char **)t13);
    t15 = (t14 + 56U);
    t16 = *((char **)t15);
    *((unsigned char *)t16) = (unsigned char)3;
    xsi_driver_subsequent_trans_delta(t9, 0U, 1, t12);
    t17 = (t0 + 3248U);
    t18 = *((char **)t17);
    t19 = *((int64 *)t18);
    t20 = (13.500000000000000 * t19);
    t17 = (t0 + 7232);
    t21 = (t17 + 56U);
    t22 = *((char **)t21);
    t23 = (t22 + 56U);
    t24 = *((char **)t23);
    *((unsigned char *)t24) = (unsigned char)2;
    xsi_driver_subsequent_trans_delta(t17, 0U, 1, t20);
    t25 = (t0 + 7232);
    xsi_driver_intertial_reject(t25, t4, t4);

LAB2:
LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_3084656879_2372691052_p_7(char *t0)
{
    char *t1;
    char *t2;
    int64 t3;
    int64 t4;
    char *t5;
    char *t6;
    char *t7;
    char *t8;
    char *t9;
    char *t10;
    int64 t11;
    int64 t12;
    char *t13;
    char *t14;
    char *t15;
    char *t16;
    char *t17;
    char *t18;
    int64 t19;
    int64 t20;
    char *t21;
    char *t22;
    char *t23;
    char *t24;
    char *t25;

LAB0:    xsi_set_current_line(114, ng0);

LAB3:    t1 = (t0 + 3248U);
    t2 = *((char **)t1);
    t3 = *((int64 *)t2);
    t4 = (0.010000000000000000 * t3);
    t1 = (t0 + 7296);
    t5 = (t1 + 56U);
    t6 = *((char **)t5);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)2;
    xsi_driver_first_trans_delta(t1, 0U, 1, t4);
    t9 = (t0 + 3248U);
    t10 = *((char **)t9);
    t11 = *((int64 *)t10);
    t12 = (5 * t11);
    t9 = (t0 + 7296);
    t13 = (t9 + 56U);
    t14 = *((char **)t13);
    t15 = (t14 + 56U);
    t16 = *((char **)t15);
    *((unsigned char *)t16) = (unsigned char)3;
    xsi_driver_subsequent_trans_delta(t9, 0U, 1, t12);
    t17 = (t0 + 3248U);
    t18 = *((char **)t17);
    t19 = *((int64 *)t18);
    t20 = (8.4000000000000004 * t19);
    t17 = (t0 + 7296);
    t21 = (t17 + 56U);
    t22 = *((char **)t21);
    t23 = (t22 + 56U);
    t24 = *((char **)t23);
    *((unsigned char *)t24) = (unsigned char)2;
    xsi_driver_subsequent_trans_delta(t17, 0U, 1, t20);
    t25 = (t0 + 7296);
    xsi_driver_intertial_reject(t25, t4, t4);

LAB2:
LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_3084656879_2372691052_p_8(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;

LAB0:    xsi_set_current_line(115, ng0);

LAB3:    t1 = (t0 + 7360);
    t2 = (t1 + 56U);
    t3 = *((char **)t2);
    t4 = (t3 + 56U);
    t5 = *((char **)t4);
    *((unsigned char *)t5) = (unsigned char)2;
    xsi_driver_first_trans_fast(t1);

LAB2:
LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_3084656879_2372691052_p_9(char *t0)
{
    char *t1;
    char *t2;
    int64 t3;
    char *t4;
    int64 t5;

LAB0:    t1 = (t0 + 6464U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(122, ng0);
    t3 = (730000 * 1000LL);
    t2 = (t0 + 6272);
    xsi_process_wait(t2, t3);

LAB6:    *((char **)t1) = &&LAB7;

LAB1:    return;
LAB4:    xsi_set_current_line(124, ng0);
    t2 = (t0 + 3248U);
    t4 = *((char **)t2);
    t3 = *((int64 *)t4);
    t5 = (t3 * 73000);
    t2 = (t0 + 6272);
    xsi_process_wait(t2, t5);

LAB10:    *((char **)t1) = &&LAB11;
    goto LAB1;

LAB5:    goto LAB4;

LAB7:    goto LAB5;

LAB8:    xsi_set_current_line(128, ng0);

LAB14:    *((char **)t1) = &&LAB15;
    goto LAB1;

LAB9:    goto LAB8;

LAB11:    goto LAB9;

LAB12:    goto LAB2;

LAB13:    goto LAB12;

LAB15:    goto LAB13;

}


extern void work_a_3084656879_2372691052_init()
{
	static char *pe[] = {(void *)work_a_3084656879_2372691052_p_0,(void *)work_a_3084656879_2372691052_p_1,(void *)work_a_3084656879_2372691052_p_2,(void *)work_a_3084656879_2372691052_p_3,(void *)work_a_3084656879_2372691052_p_4,(void *)work_a_3084656879_2372691052_p_5,(void *)work_a_3084656879_2372691052_p_6,(void *)work_a_3084656879_2372691052_p_7,(void *)work_a_3084656879_2372691052_p_8,(void *)work_a_3084656879_2372691052_p_9};
	xsi_register_didat("work_a_3084656879_2372691052", "isim/Contador_reloj_tb_isim_beh.exe.sim/work/a_3084656879_2372691052.didat");
	xsi_register_executes(pe);
}
