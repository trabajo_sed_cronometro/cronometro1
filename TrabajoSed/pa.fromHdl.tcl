
# PlanAhead Launch Script for Pre-Synthesis Floorplanning, created by Project Navigator

create_project -name TrabajoSed -dir "C:/Users/sed/Desktop/trabajo_sed_cronometro-cronometro1-443f97fa434f/TrabajoSed/planAhead_run_2" -part xc3s200ft256-5
set_param project.pinAheadLayout yes
set srcset [get_property srcset [current_run -impl]]
set_property target_constrs_file "reloj.ucf" [current_fileset -constrset]
set hdlfile [add_files [list {siete_segmentos_mux.vhd}]]
set_property file_type VHDL $hdlfile
set_property library work $hdlfile
set hdlfile [add_files [list {siete_segmentos.vhd}]]
set_property file_type VHDL $hdlfile
set_property library work $hdlfile
set hdlfile [add_files [list {clk200Hz.vhd}]]
set_property file_type VHDL $hdlfile
set_property library work $hdlfile
set hdlfile [add_files [list {siete_segmentos_completo.vhd}]]
set_property file_type VHDL $hdlfile
set_property library work $hdlfile
set hdlfile [add_files [list {Debouncers.vhd}]]
set_property file_type VHDL $hdlfile
set_property library work $hdlfile
set hdlfile [add_files [list {contador_reloj.vhd}]]
set_property file_type VHDL $hdlfile
set_property library work $hdlfile
set hdlfile [add_files [list {clk1Hz.vhd}]]
set_property file_type VHDL $hdlfile
set_property library work $hdlfile
set hdlfile [add_files [list {reloj.vhd}]]
set_property file_type VHDL $hdlfile
set_property library work $hdlfile
set_property top reloj $srcset
add_files [list {reloj.ucf}] -fileset [get_property constrset [current_run]]
open_rtl_design -part xc3s200ft256-5
