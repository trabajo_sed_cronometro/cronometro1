
# PlanAhead Launch Script for Post-Synthesis pin planning, created by Project Navigator

create_project -name TrabajoSed -dir "C:/Users/sed/Desktop/trabajo_sed_cronometro-cronometro1-443f97fa434f/TrabajoSed/planAhead_run_2" -part xc3s200ft256-5
set_property design_mode GateLvl [get_property srcset [current_run -impl]]
set_property edif_top_file "C:/Users/sed/Desktop/trabajo_sed_cronometro-cronometro1-443f97fa434f/TrabajoSed/reloj.ngc" [ get_property srcset [ current_run ] ]
add_files -norecurse { {C:/Users/sed/Desktop/trabajo_sed_cronometro-cronometro1-443f97fa434f/TrabajoSed} }
set_param project.pinAheadLayout  yes
set_property target_constrs_file "reloj.ucf" [current_fileset -constrset]
add_files [list {reloj.ucf}] -fileset [get_property constrset [current_run]]
link_design
