----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    13:20:31 12/12/2016 
-- Design Name: 
-- Module Name:    reloj - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
-- Descripci�n: 
--   Une el contador del reloj con los divisores de frecuencia y el controlador 
--   de siete segmentos completo para mostrar la hora en una tarjeta Basys2.
entity reloj is
    Port ( clk : in  STD_LOGIC;
           salida : out  STD_LOGIC_VECTOR(7 downto 0);
           MUX : out  STD_LOGIC_VECTOR(3 downto 0);
			  bb1 : in  STD_LOGIC;
           bb2 : in  STD_LOGIC;
           bb3 : in  STD_LOGIC;
           bb4 : in  STD_LOGIC;
			  bpausa: in std_logic;
           breset : in  STD_LOGIC;
           bdown : in  STD_LOGIC;
			  bseleccion: in std_logic
		);
end reloj;

architecture Behavioral of reloj is
	COMPONENT clk1Hz IS
		PORT (
            entrada: IN  STD_LOGIC;
            reset  : IN  STD_LOGIC;
            salida : OUT STD_LOGIC
        );
		END COMPONENT;
	Component Debouncers is
		Port ( clock: in std_logic;
				bb1 : in  STD_LOGIC;           
				b1s : out  STD_LOGIC);
		end component;
		
	COMPONENT contador_reloj IS
		PORT (
			clk  : IN  STD_LOGIC;
			reset: IN  STD_LOGIC;
			down : in std_logic;
			pausa: in std_logic;
			seleccion: in std_logic;
			b1: in std_logic;
			b2: in std_logic;
			b3: in std_logic;
			b4: in std_logic;
			M1   : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
			M0   : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
			S1   : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
			S0   : OUT STD_LOGIC_VECTOR(3 DOWNTO 0)
		);
	END COMPONENT;
	
	COMPONENT siete_segmentos_completo IS
		PORT (
			clk   : IN  STD_LOGIC;
			reset : IN  STD_LOGIC;
			D0    : IN  STD_LOGIC_VECTOR(3 downto 0);
			D1    : IN  STD_LOGIC_VECTOR(3 downto 0);
			D2    : IN  STD_LOGIC_VECTOR(3 downto 0);
			D3    : IN  STD_LOGIC_VECTOR(3 downto 0);
			salida: OUT STD_LOGIC_VECTOR(7 downto 0);
			MUX   : OUT STD_LOGIC_VECTOR(3 downto 0)

		);
	END COMPONENT;
	
	signal clk_out : STD_LOGIC := '0';
	signal MM1, SS1: STD_LOGIC_VECTOR(2 downto 0);
	signal MM0,SS0: STD_LOGIC_VECTOR(3 downto 0);
	signal pMM1, pMM0, pSS1, pSS0: STD_LOGIC_VECTOR(3 downto 0);
	signal pb1s,pb2s,pb3s,pb4s,preset,pdown, ppausa, pseleccion: std_logic:='0';
begin
	--PORT MAPs necesarios para habilitar el reloj.
	clk_i: clk1Hz PORT MAP(clk, preset, clk_out);
	b1_debouncer: Debouncers Port MAp(clk, bb1, pb1s);	
	b2_debouncer: Debouncers Port MAp(clk, bb2, pb2s);
	b3_debouncer: Debouncers Port MAp(clk, bb3, pb3s);
	b4_debouncer: Debouncers Port MAp(clk, bb4, pb4s);
	down_debouncer: Debouncers Port MAp(clk, bdown, pdown);
	reset_debouncer: Debouncers Port MAp(clk, breset, preset);
	seleccion_debouncer: Debouncers Port MAp(clk, bseleccion, pseleccion);
	pausa_debouncer: Debouncers Port MAp(clk, bpausa, ppausa);
	cnt_i: contador_reloj PORT MAP(clk_out,preset, pdown,ppausa, pseleccion, pb1s, pb2s, pb3s, pb4s, MM1, MM0, SS1, SS0);
	seg_i: siete_segmentos_completo PORT MAP(clk, preset, pSS0, pSS1, pMM0, pMM1, salida, MUX);
	
	--Padding de las se�ales del contador para siete segmentos.
	pMM1 <= "0" & MM1;
	pMM0 <= MM0;
	pSS1 <= "0" & SS1;
	pSS0 <= SS0;

end Behavioral;

