
-- VHDL Instantiation Created from source file reloj.vhd -- 22:38:40 01/17/2017
--
-- Notes: 
-- 1) This instantiation template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the instantiated module
-- 2) To use this template to instantiate this entity, cut-and-paste and then edit

	COMPONENT reloj
	PORT(
		clk : IN std_logic;
		bb1 : IN std_logic;
		bb2 : IN std_logic;
		bb3 : IN std_logic;
		bb4 : IN std_logic;
		bpausa : IN std_logic;
		breset : IN std_logic;
		bdown : IN std_logic;
		bseleccion : IN std_logic;          
		salida : OUT std_logic_vector(7 downto 0);
		MUX : OUT std_logic_vector(3 downto 0)
		);
	END COMPONENT;

	Inst_reloj: reloj PORT MAP(
		clk => ,
		salida => ,
		MUX => ,
		bb1 => ,
		bb2 => ,
		bb3 => ,
		bb4 => ,
		bpausa => ,
		breset => ,
		bdown => ,
		bseleccion => 
	);


