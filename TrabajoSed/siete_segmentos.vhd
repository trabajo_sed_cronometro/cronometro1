----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    13:34:11 12/12/2016 
-- Design Name: 
-- Module Name:    siete_segmentos - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
-- Descripci�n: 
--   Decodificador de seis bits a siete segmentos. Se incluyen los n�meros del 0
--   al 9, las letras de la A a la Z, y otros signos de puntuaci�n utilizados.
--
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
 
entity siete_segmentos is
    PORT (
        entrada: IN  STD_LOGIC_VECTOR(3 downto 0);
        salida : OUT STD_LOGIC_VECTOR(7 downto 0)
    );
end siete_segmentos;
 
architecture Behavioral of siete_segmentos is
begin
    visualizador: process (entrada) begin
        case entrada is
            when "0000" =>  salida <= x"03"; -- 0
            when "0001" =>  salida <= x"9F"; -- 1
            when "0010" =>  salida <= x"25"; -- 2
            when "0011" =>  salida <= x"0D"; -- 3
            when "0100" =>  salida <= x"99"; -- 4
            when "0101" =>  salida <= x"49"; -- 5
            when "0110" =>  salida <= x"41"; -- 6
            when "0111" =>  salida <= x"1F"; -- 7
            when "1000" =>  salida <= x"01"; -- 8
            when "1001" =>  salida <= x"19"; -- 9
            when others   =>  salida <= x"FF"; -- Nada
        end case;
    end process;
end Behavioral;