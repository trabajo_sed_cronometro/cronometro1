----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    13:39:13 12/12/2016 
-- Design Name: 
-- Module Name:    siete_segmentos_mux - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
-- Descripci�n: 
--   Multiplexor (de frecuencia) para mostrar un valor diferente en cada uno de
--   los visualizadores de siete segmentos. Esto se logra activando solamente un
--   visualizador a la vez y mandar el dato correspondiente. Si la frecuencia es
--   mayor a 16Hz por visualizador, no habr� parpadeo perceptible.

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
 
entity siete_segmentos_mux is
    PORT (
        clk   : IN  STD_LOGIC;
        reset : IN  STD_LOGIC;
        D0    : IN  STD_LOGIC_VECTOR(3 downto 0);  --Primer d�gito.
        D1    : IN  STD_LOGIC_VECTOR(3 downto 0);  --Segundo d�gito.
        D2    : IN  STD_LOGIC_VECTOR(3 downto 0);  --Tercer d�gito.
        D3    : IN  STD_LOGIC_VECTOR(3 downto 0);  --Cuarto d�gito.
        salida: OUT STD_LOGIC_VECTOR(3 downto 0);  --Salida del multiplexor (valor a desplegar).
        MUX   : OUT STD_LOGIC_VECTOR(3 downto 0)  --Valor que define cual d�gito se va a mostrar.

    );
end siete_segmentos_mux;
 
architecture Behavioral of siete_segmentos_mux is
    type estados is (rst, v0, v1, v2, v3);
    signal estado : estados;
begin
    visualizadores: process (reset, clk) begin
        if (reset = '1') then
            estado <= rst;
            MUX <= "0000";
            salida <= "0000";
        elsif rising_edge(clk) then
            case estado is
                when v0 =>
                    salida <= D3;
                    MUX <= "0111"; -- Display de las decenas de minutos
                    estado <= v1;
                when v1 =>
                    salida <= D2; -- Display de las unidades de minutos
                    MUX <= "1011";
                    estado <= v2;
                when v2 =>
                    salida <= D1;
                    MUX <= "1101"; -- Display de las decenas de segundos
                    estado <= v3;
                when v3 =>
                    salida <= D0;
                    MUX <= "1110"; -- Display de las unidades de segunods
                    estado <= v0;
					when rst =>
                    salida <= D0;
                    MUX <= "0000";
                    estado <= v0;
            end case;
        end if;
    end process;
end Behavioral;