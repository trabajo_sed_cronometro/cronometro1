--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   17:42:49 12/29/2016
-- Design Name:   
-- Module Name:   C:/Users/jorge/cronometro1/TrabajoSed/siete_segmentos_mux_tb.vhd
-- Project Name:  TrabajoSed
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: siete_segmentos_mux
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY siete_segmentos_mux_tb IS
END siete_segmentos_mux_tb;
 
ARCHITECTURE behavior OF siete_segmentos_mux_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT siete_segmentos_mux
    PORT(
         clk : IN  std_logic;
         reset : IN  std_logic;
         D0 : IN  std_logic_vector(3 downto 0);
         D1 : IN  std_logic_vector(3 downto 0);
         D2 : IN  std_logic_vector(3 downto 0);
         D3 : IN  std_logic_vector(3 downto 0);
         salida : OUT  std_logic_vector(3 downto 0);
         MUX : OUT  std_logic_vector(3 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal reset : std_logic := '0';
   signal D0 : std_logic_vector(3 downto 0) := (others => '0');
   signal D1 : std_logic_vector(3 downto 0) := (others => '0');
   signal D2 : std_logic_vector(3 downto 0) := (others => '0');
   signal D3 : std_logic_vector(3 downto 0) := (others => '0');

 	--Outputs
   signal salida : std_logic_vector(3 downto 0);
   signal MUX : std_logic_vector(3 downto 0);

   -- Clock period definitions
   constant clk_period : time := 5000000 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: siete_segmentos_mux PORT MAP (
          clk => clk,
          reset => reset,
          D0 => D0,
          D1 => D1,
          D2 => D2,
          D3 => D3,
          salida => salida,
          MUX => MUX
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      reset<='1';
		D0<="0000";
		D1<="0101";
		D2<="0011";
		D3<="0111";
		
		
		wait for 100 ns;
		reset<='0';
		
				

      --wait for clk_period*10;
		      -- insert stimulus here 

      wait;
   end process;

END;
