--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   16:36:54 12/29/2016
-- Design Name:   
-- Module Name:   C:/Users/jorge/cronometro1/TrabajoSed/siete_segmentos_tb.vhd
-- Project Name:  TrabajoSed
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: siete_segmentos
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY siete_segmentos_tb IS
END siete_segmentos_tb;
 
ARCHITECTURE behavior OF siete_segmentos_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT siete_segmentos
    PORT(
         entrada : IN  std_logic_vector(3 downto 0);
         salida : OUT  std_logic_vector(7 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal entrada : std_logic_vector(3 downto 0) := (others => '0');

 	--Outputs
   signal salida : std_logic_vector(7 downto 0);
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
   --constant <clock>_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: siete_segmentos PORT MAP (
          entrada => entrada,
          salida => salida
        ); 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
     
		entrada<="0000";

		wait for 100 ns;
		entrada<="0111";

		wait for 120 ns;
		entrada<="0101";

		wait for 200 ns;
		entrada<="0011";		

      --wait for <clock>_period*10;

      -- insert stimulus here 

      wait;
   end process;

END;